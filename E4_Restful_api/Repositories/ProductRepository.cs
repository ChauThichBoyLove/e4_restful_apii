﻿namespace E4_Restful_api.Repositories
{
    public class ProductRepository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}
